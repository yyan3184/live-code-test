/**
 * Finds the indexes of the countNum largest items in an array.
 * Example:
 *   items:    [90,19,67,12,92,13,41]
 *   countNum: 4
 *   Result:   [0,2,4,6]
 *
 * @param {Number[]} items Input array of numbers
 * @param {Number} countNum The number of the largest items in the array to return
 * @return {Number[]} An array of the indexes of the largest items in the input array
 */
function getMaxItems(items, countNum) {
  const mapping = {};
  for (let i = 0; i < items.length; i++) {
    mapping[items[i]] = i;
  }
  // {90: 0, 19:1, 67:2}

  const sorted = items.sort((a, b) => b - a);

  const output = sorted.map((element) => {
    return mapping[element];
  });
  return output.slice(0, countNum);
}

module.exports = getMaxItems;
