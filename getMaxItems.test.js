const getMaxItems = require('./getMaxItems.js');

test('example test', () => {
  expect(getMaxItems([90,19,67,12,92,13,41], 4)).toStrictEqual([0,2,4,6]);
});